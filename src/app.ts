import express from 'express'
import AWS from 'aws-sdk'

AWS.config.update({ region: 'us-east-2' })

AWS.config.getCredentials((error) => {
  if (error) {
    console.log(error)
  } else {
    console.log('Access Keys: ', AWS.config.credentials?.accessKeyId)
  }
})

const app = express()

const port = 8000

app.use(express.json())

app.get('/', (_, req) => {
  req.send('<h1>hello world</h1>')
})

app.post<unknown, unknown, { to: string[]; subject: string; message: string }>(
  '/email',
  async (req, res) => {
    new AWS.SES({ apiVersion: 'latest' })
      .sendEmail({
        Destination: { ToAddresses: req.body.to },
        Message: {
          Subject: {
            Charset: 'UTF-8',
            Data: req.body.subject,
          },
          Body: {
            Text: {
              Charset: 'UTF-8',
              Data: req.body.message,
            },
            // Html: {
            //   Charset: 'UTF-8',
            //   Data: '<h1>hello from AWS SES</h1>',
            // },
          },
        },
        Source: 'chentselee@gmail.com',
      })
      .promise()
      .then((data) => {
        console.log({ data })
        res.sendStatus(200)
      })
      .catch((error) => {
        console.log({ error })
        res.sendStatus(400)
      })
  }
)

app.listen(port, () => {
  console.log(`Server listening on port ${port}`)
})
