# [AWS SES(Simple Mail Service)](https://aws.amazon.com/ses) Survey

## Docs

- [JavaScript SDK](https://docs.aws.amazon.com/zh_tw/sdk-for-javascript/v2/developer-guide/ses-examples-sending-email.html)
- [Go SDK](https://docs.aws.amazon.com/sdk-for-go/v1/developer-guide/using-ses-with-go-sdk.html)

## Prerequisite

- AWS Credentials
- AWS SDK

## Notes

1. Supports sending text and html.
2. The demo api only supports sending one addres at a time but the SDK supports sending **up to 50** addresses per operation. Any more addresses will need to be divided into groups of 50.
3. The demo uses [**sandbox mode**](https://docs.aws.amazon.com/ses/latest/dg/request-production-access.html) which only allows sending to and from verified email addresses. For production, you wound need to enable production mode.
4. 62000 emails per month (sent from AWS EC2 instance). See [pricing details](https://aws.amazon.com/tw/ses/pricing/).

## Steps before production

1. [Move out of sandbox mode](https://docs.aws.amazon.com/ses/latest/dg/request-production-access.html)
2. [Remove throttle on port 25 for EC2 instance](https://aws.amazon.com/tw/premiumsupport/knowledge-center/ec2-port-25-throttle/)

## API References

- POST /email
  - body
    - to **Array<string>** valid email address
    - subject **string** email subject
    - message **string** message to send
